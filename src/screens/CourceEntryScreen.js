import React, { useState } from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {Heading} from '../components/Heading';
import {AwesomeTextInput} from '../components/AwesomeTextInput';
import {AwesomeButton} from '../components/AwesomeButton';

export const CourceEntryScreen = (props) => {
    const [name, setName] = useState('');
    const [phone, setPhone] = useState('');
    const [category, setCategory] = useState('')
    return(
        <View style={styles.conteiner}>
            <View style={styles.heading}>
                <Heading title={`ЗАПИСЬ${'\n'}НА КУРСЫ`}/>
            </View>
            <Text style={styles.smallText}>Заполните для того, чтобы забронировать место на курсе в автошколе, и мы связались с вами для подтверждения записи.</Text>
            <View style={styles.entryForm}>
                <AwesomeTextInput
                    placeholder='ФИО'
                    setInput={setName}
                />
                <AwesomeTextInput
                    placeholder='Номер телефона'
                    setInput={setPhone}
                />
                <AwesomeTextInput
                    placeholder='Желаемая категория'
                    setInput={setCategory}
                />
            </View>
            <View style={styles.button}>
                <AwesomeButton
                    text='ЗАПИСАТЬСЯ'
                    onPress = {() => pressButton(name, phone, category, props)}
                />
            </View>
        </View>
    );
}

const pressButton = (name, phone, category, props) => {
    props.navigation.navigate('SignIn');
}
const styles = StyleSheet.create({
    conteiner: {
        flex: 1,
        backgroundColor: '#FFF',
        alignItems: 'center'
    },
    heading: {
        height: '20%',
        justifyContent: 'flex-end',
        paddingBottom: '3%',
    },
    smallText: {
        width: '80%',
        textAlign: 'center',
        fontFamily: 'montserrat',
        fontSize: 17,
        height: '16%',
    },
    entryForm: {
        height: '27%',
        width: '80%',
        justifyContent: 'space-between'
    },
    button: {
        height: '20%',
        width: '80%',
        justifyContent: 'center',
    }
})