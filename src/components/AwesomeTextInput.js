import React, { Component } from 'react';
import {StyleSheet, TextInput, View } from 'react-native';

export const AwesomeTextInput = (props) => {
    return (
        <View style={styles.container}>
            <TextInput 
                placeholder={props.placeholder}
                secureTextEntry={props.secureTextEntry}
                onChangeText ={props.setInput}
                style={styles.input}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%'
    },
    input: {
        backgroundColor: 'white',
        paddingVertical:19,
        paddingHorizontal: 30,
        fontSize: 14,
        fontFamily: 'montserrat',
        color: '#000',
        height: 56,
        elevation: 3
    }
})