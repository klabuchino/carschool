import React, { useState } from 'react';
import {StyleSheet, View, Text} from 'react-native';

export const MenuScreen = (props) => {
    return (
        <Text onPress={() => props.navigation.goBack()}>Back</Text>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'blue',
        justifyContent: 'center',
        height: '100%',
        width: '50%'
    }
})