import React from 'react';
import { View, Text } from 'react-native';
import { createAppContainer, createSwitchNavigator} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {SignInScreen} from '../screens/SignInScreen';
import {CourceEntryScreen} from '../screens/CourceEntryScreen';
import Menu from '../navigation/BurgerMenuNavigator';
import { MainScreen } from '../screens/MainScreen';

const auth = createStackNavigator(
    {
        SignIn: SignInScreen,
        LogIn: CourceEntryScreen
    },
    {
        initialRouteName: 'SignIn',
        headerMode: 'none',
        mode: "card"
    }
)
const appNavigator = createSwitchNavigator(
    {
        Main: Menu,
        Auth: auth
    },
    {
        initialRouteName: 'Auth'
    }
)
export default createAppContainer(appNavigator);
