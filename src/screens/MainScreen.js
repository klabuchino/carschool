import React, { useState } from 'react';
import {StyleSheet, Image, View, Text, TouchableOpacity} from 'react-native'; 
import {Notification} from '../components/Notification';
import {NotificationEntry} from '../components/NotificationEntry';

export const MainScreen = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.buttonMenu}>
                <TouchableOpacity
                    onPress={props.navigation.openDrawer}
                    style={styles.icon}
                >
                    <Image source={require('../../assets/icons/menu.png')} style={styles.icon}/>
                </TouchableOpacity>
            </View>
            <Image
                style={styles.image}
                source={require('../../assets/images/Car.png')}
            />
            <View style={styles.containerLink}>
                <TouchableOpacity style={styles.link}>
                    <Image source={require('../../assets/icons/tests.png')} style={styles.icon}/>
                    <Text style={styles.textLink}>ТЕСТЫ ПДД</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.link}>
                    <Image source={require('../../assets/icons/tickets.png')} style={styles.icon}/>
                    <Text style={styles.textLink}>БИЛЕТЫ ПДД</Text>
                </TouchableOpacity>
            </View>
            <Notification text='4 ноября в 20:00 занятия по теоритической части'/>
            <NotificationEntry date='03.12.2020' nameInstructor='Иванов Иван Иванович'/>
        </View>
    )
}
MainScreen.navigationOptions = {
    title: 'Главная страница',
    drawerIcon: ({ tintColor }) => {
        return (
        <Image
            source={require('../../assets/icons/home.png')}
            style={[{width: 23, height: 23}, {tintColor: tintColor}]}
          />)
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
        alignItems: 'center'
    },
    icon: {
        height: 18,
        width: 20,
    },
    image: {
        marginTop: 50
    },
    buttonMenu: {
        width: '85%',
        marginTop: 30
    },
    containerLink: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-around',
        marginTop: 50,
        height: 70,
        borderBottomColor: '#F1F1F1',
        borderBottomWidth: 2
    },
    link: {
        flexDirection: 'row'
    },
    textLink: {
        fontFamily: 'montserrat-bold',
        fontSize: 16,
        marginLeft: 5  
    }
})