import {types} from '../../reduxTypes';

const initalState = {
    me: {}
}

export const userRedusers = (state = initalState, action) => {
    const {type, payload} = action;

    switch (type) {
        case types.AUTH: {
            return {
                ...state,
                me: payload
            }
        }
        case types.AUTH_ERROR: {
            return {
                ...state,
                error: payload
            }
        }
    }
}