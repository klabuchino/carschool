import React from 'react';
import {StyleSheet, Image, View, Text, TouchableOpacity} from 'react-native'; 

export const NotificationEntry = props => {
    return (
        <View style={styles.container}>
            <View style={styles.notification}>
                <Image
                    source={require('../../assets/icons/entry.png')}
                    style={styles.image}
                />
                <Text style={styles.message}>Запись на вождение на {props.date}. Инструктор: {props.nameInstructor}. </Text>
            </View>
            <View style={{flexDirection: 'row',justifyContent: 'flex-end'}}>
                <TouchableOpacity
                    onPress={props.onPressButton}
                    style={styles.button}
                >
                    <Text style={{color: '#FFF', fontFamily: 'montserrat', fontSize: 12}}>Отменить</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        borderBottomWidth: 1,
        borderBottomColor: '#F1F1F1',
        height: 100,
        justifyContent: 'center',
    },
    notification: {
        flexDirection: 'row',
        width: '90%',
        alignItems: 'center',
    },
    image: {
        height: 30, 
        width: 31,
        marginLeft: 15
    },
    message: {
        marginLeft: 10,
        fontSize: 14,
        fontFamily: 'montserrat',
        marginRight: 20
    },
    button: {
        backgroundColor: '#FF8C04',
        width: 90,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 8,
        marginRight: 20
    }
})