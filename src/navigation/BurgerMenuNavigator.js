import {createDrawerNavigator, DrawerItems} from 'react-navigation-drawer';
import {MainScreen} from '../screens/MainScreen';
import {MenuScreen} from '../screens/MenuScreen';
import {Screen} from '../screens/Screen';
import SafeAreaView from 'react-native-safe-area-view';
import React from 'react';
import { View, ScrollView, StyleSheet, Text} from 'react-native';

const CustomDrawerContentComponent = props => (
    <ScrollView>
        <SafeAreaView
            style={styles.container}
            forceInset={{ top: 'always', horizontal: 'never' }}
        >
            <View style={styles.header}>
                <View style={styles.line}>
                    <Text style={styles.nameField}>ФИО:</Text>
                    <Text style={styles.field}>{props.name}Александр</Text>
                </View>
                <View style={styles.line}>
                    <Text style={styles.nameField}>Группа:</Text>
                    <Text style={styles.field}>{props.group}ФО-460001</Text>
                </View>
                <View style={styles.line}>
                    <Text style={styles.nameField}>Категория:</Text>
                    <Text style={styles.field}>{props.category}В</Text>
                </View>
            </View>
            <DrawerItems {...props}/>
        </SafeAreaView>
    </ScrollView>
);
const styles = StyleSheet.create({
    container: {
        flex: 1       
    },
    header:{
        justifyContent: 'center',
        height: 150,
        paddingHorizontal: 15,
        borderColor: '#F1F1F1',
        borderBottomWidth: 1,
    },
    nameField: {
        fontFamily: 'montserrat-bold',
        fontSize: 18
    },
    field: {
        fontFamily: 'montserrat',
        fontSize: 18,
        marginLeft: 5
    },
    line: {
        flexDirection: "row"
    }
})
const contentOptions = {
    activeBackgroundColor: '#F1F1F1',
    activeTintColor: '#008B8B',
    itemsContainerStyle: {
        paddingVertical: 0
    },
    itemStyle: { 
        borderBottomWidth: 1,
        borderColor: '#F1F1F1',
    },
    labelStyle: {
        marginLeft: 0,
        fontWeight: 'normal',
        fontFamily: 'montserrat-bold',
    }
}
export default createDrawerNavigator(
    {
        Main: {
            screen: MainScreen,
            navigationOptions: MainScreen.navigationOptions
        },
        Menu: {
            screen: MenuScreen,
        },
        Screen: {
            screen: Screen,
        },
    },
    {
        drawerWidth: '83%',
        edgeWidth: 200,
        contentComponent: CustomDrawerContentComponent,
        contentOptions: contentOptions
    }
)