var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var crypto = require('crypto');
var async = require('async');
var config = require('../config.js');
var _ = require('lodash');
var debug = require('debug')('Uvolunteer:server');

var schema = new Schema({
    phoneNumber: {
      type: String,
      default: ''
    },
    hashedPassword: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    fullName: {
        type: String,
        default: ''
    },
    category: {
        type: String,
        default: ''
    },
    group: {
        type: String,
        default: ''
    }
})
schema.methods.encryptPassword = function (password) {
  return crypto.createHmac('sha1', this.salt).update(config.appSalt).update(password).digest('hex');
};

schema.virtual('password')
  .set(function (password) {

    this._plainPassword = password;
    console.log('password: ', password);

    this.salt = Math.random() + '';
    this.hashedPassword = this.encryptPassword(password);
  })
  .get(function () { return this._plainPassword; });



schema.methods.checkPassword = function (password) {
  return this.encryptPassword(password) === this.hashedPassword;
};



schema.statics.authorize = function (phoneNumber, password, callback) {

  var BaseUser = this;

  async.waterfall([
    function (done) {
      BaseUser.findOne({ phoneNumber: phoneNumber}, done);
    },
    function (user, done) {
      if (user) {
        if (user.checkPassword(password)) {

          if (user) {
            user = user.toObject();
            delete user.hashedPassword;
            delete user.salt;
          }
          console.log('USER::: ', user);

          done(null, user);
        } else {
          done("Пользователь не найден");
        }
      } else {
        done("Пользователь не найден");
      }
    }
  ], callback);
};

schema.statics.createNew = function (phoneNumber, obj, callback) {
  var BaseUser = this;
  async.waterfall([
    function (done) {
      BaseUser.findOne({ phoneNumber: phoneNumber}, function (err, usr) {
        done(null, usr);
      });
    },
    function (user, done) {
      if (user) {
        done(null, "Пользователь с таким именем существует");
      } else {

        var user = new BaseUser(obj);
        console.log(user);
        user.save(function (err, usr) {
          console.log(usr);
          if (err) return callback(err);

          // console.log('user: ', user);


          if (usr) {
            user = user.toObject();
            delete user.hashedPassword;
            delete user.salt;
          }

          done(null, user);
        });
      }
    }
  ], function (err, user) {
    callback(err, user);
  });
};

exports.BaseUser = BaseUser = mongoose.model('BaseUser', schema);