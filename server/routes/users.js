var express = require('express');
var router = express.Router();
var passport = require('passport');
var BaseUser = require('../models/BaseUser.js').BaseUser;

router.post('/createUser', (req, res, next) => {
    if(Object.keys(req.body).length > 0) {
      BaseUser.createNew(req.body.phoneNumber, req.body, (err, user) => {
        console.log(err);
        res.json(user);
      })
    } else res.json({error: 'not_params'})
})

router.post('/login', function(req, res, next) {
  console.log('body:', req.body);
  console.log('query:', req.query);

  passport.authenticate('local',
      function (err, user, info) {
          console.log('user:',user);
          return err
              ? res.json({ error: "failed error" })
              : user
                  ? req.logIn(user, function (err) {
                      return err
                          ? next(err)
                          : res.json({
                                data: {
                                    user: user
                                }
                            });
                  })
                  : res.json({error: "failed not user"});
      }
  )(req, res, next);
});

router.post('/logout', function(req, res, next) {
  req.logout();
  res.json({data: "success logout"});
});

module.exports = router;
