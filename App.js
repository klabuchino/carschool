import * as Font from 'expo-font';
import {AppLoading} from 'expo';
import React, { useState } from 'react';
import {StyleSheet, View, Text, KeyboardAvoidingView} from 'react-native';
import AppContainer from './src/navigation/AppNavigator'
import { Provider } from 'react-redux';
import { createStore } from 'redux';

const store = createStore(allRedusers);

export default function App() {
    const [isLoadingComplete, setLoadingComplete] = useState(false);

    if(!isLoadingComplete) {
      return (
        <AppLoading
        startAsync={loadingResousesAsync}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
        />
      );
    }
    else {
      return (
        <Provider>
          <AppContainer/>
        </Provider>
      );
    }
}

async function loadingResousesAsync() {
  await Font.loadAsync({
    'montserrat': require('./assets/fonts/montserrat-extraLight.ttf'),
    'montserrat-bold': require('./assets/fonts/Montserrat-Light.ttf')
 })
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})