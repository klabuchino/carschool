import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';

export const AwesomeButton = (props) => {
    return (
        <TouchableOpacity 
            style={styles.button}
            onPress={props.onPress}
        >
            <Text style={styles.text}>{props.text}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'montserrat',
        fontSize: 18,
        color: 'white'
    },
    button: {
        height: 56,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#008B8B',
        elevation: 1
    }
})