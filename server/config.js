module.exports = {
    mongoUrl: "mongodb://localhost/CarSchool",
    session: {
        secret: 'Tf64576ygvH5fr%$%^&uh',
        name: 'CarSchool.sid',
        proxy: true,
        resave: true,
        saveUninitialized: true,
        //cookie: { secure: true }
    },
    appSalt: "hb^hbfG^4F5tfgG56tgfr&8y6tgNmj",
}