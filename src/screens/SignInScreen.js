import React, { useState } from 'react';
import {StyleSheet, View, Text, KeyboardAvoidingView} from 'react-native';
import {Heading} from '../components/Heading';
import {AwesomeTextInput} from '../components/AwesomeTextInput';
import {AwesomeButton} from '../components/AwesomeButton';
import {Link} from 'react-router-native'

export const SignInScreen =(props) => {

    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');

    return (
        <KeyboardAvoidingView style={styles.conteiner} behavior='padding' enabled>
            <View style={styles.welcome}>
              <Heading title={`ДОБРО${'\n'}ПОЖАЛОВАТЬ`}/>
            </View>
              <View style={styles.inputForm}>
                <AwesomeTextInput 
                    placeholder='Логин'
                    secureTextEntry={false}
                    setInput={setLogin}
                />
                <AwesomeTextInput 
                    placeholder='Пароль' 
                    secureTextEntry={true}
                    setInput={setPassword}
                />
              </View>
              <View style={styles.button}>
                <AwesomeButton 
                    text='ВОЙТИ'
                    onPress={() => pressButtonSignIn(login, password, props)}
                />
              </View>
                <Text 
                style={styles.bottomText}
                onPress={() =>pressTextEntry(props)}
                >
                  Все еще не записаны в Автошколу? Записаться.</Text>
        </KeyboardAvoidingView>
    )
}

function pressButtonSignIn(login, password, props) {
    props.navigation.navigate('Main');
}

function pressTextEntry(props) {
    props.navigation.navigate('LogIn');
}

const styles = StyleSheet.create({
    conteiner: {
      flex: 1,
      backgroundColor: '#FFF',
      alignItems: 'center',
      //justifyContent: 'center'
    },
    welcome: {
      height: '40%',
      justifyContent: 'flex-end',
      alignItems: 'center',
      paddingBottom: '5%'
    },
    inputForm: {
      height: '25%',
      width: '80%',
      justifyContent: 'space-around'
    },
    button: {
      width: '80%',
      marginTop: '13%'   
    },
    bottomText: {
      fontFamily: 'montserrat',
      fontSize: 12,
      textAlign: 'center',
      marginTop: '30%',
      textDecorationLine: 'underline'
    }
});