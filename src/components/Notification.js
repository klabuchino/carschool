import React from 'react';
import {StyleSheet, Image, View, Text, TouchableOpacity} from 'react-native'; 

export const Notification = (props) => {
    return (
        <TouchableOpacity style={styles.container}>
            <View style={styles.notification}>
                <Image
                    source={require('../../assets/icons/notification.png')}
                    style={styles.image}
                />
                <Text style={styles.message}>{props.text}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        borderBottomWidth: 1,
        borderBottomColor: '#F1F1F1',
        height: 100,
        justifyContent: 'center',
    },
    notification: {
        flexDirection: 'row',
        width: '90%',
        alignItems: 'center',
    },
    image: {
        height: 30, 
        width: 31,
        marginLeft: 15
    },
    message: {
        marginLeft: 10,
        fontSize: 14,
        fontFamily: 'montserrat',
    }
})