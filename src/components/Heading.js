import React, { Component } from 'react';
import {StyleSheet, Text, View} from 'react-native';

export const Heading = ({title}) => {
    return (
        <Text style={styles.text}>{title}</Text>
    )
}
const styles = StyleSheet.create({
    text : {
        textAlign: 'center',
        fontSize: 28,
        lineHeight: 34,
        fontFamily: 'montserrat'
      }
})